﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace RL
{
    public class UserElement:ConfigurationElement
    {
        /// <summary>
        /// Id of user
        /// </summary>
        [ConfigurationProperty("idUser", IsRequired = true)]
        public string IdUser
        {
            get { return (string)this["idUser"]; }
            set { this["idUser"] = value; }
        }

        /// <summary>
        /// Login of user
        /// </summary>
        [ConfigurationProperty("login", IsRequired = true)]
        public string Login
        {
            get { return (string)this["login"]; }
            set { this["login"] = value; }
        }

        /// <summary>
        /// Password of user
        /// </summary>
        [ConfigurationProperty("password", IsRequired = true)]
        public string Password
        {
            get { return (string)this["password"]; }
            set { this["password"] = value; }
        }

        /// <summary>
        /// Role of user
        /// </summary>
        [ConfigurationProperty("role", IsRequired = true)]
        public string Role
        {
            get { return (string)this["role"]; }
            set { this["role"] = value; }
        }
    }
}
