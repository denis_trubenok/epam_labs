﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace RL
{
    public class UserSection:ConfigurationSection
    {
        /// <summary>
        /// Collection of Users
        /// </summary>
        [ConfigurationProperty("users")]
        public UsersCollection Users
        {
            get { return (UsersCollection)this["users"]; }
        }
    }
}
