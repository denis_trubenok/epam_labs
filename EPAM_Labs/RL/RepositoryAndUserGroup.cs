﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace RL
{
    public class RepositoryAndUserGroup:ConfigurationSectionGroup
    {
        [ConfigurationProperty("repositorySection")]
        public RepositorySection RepositorySection
        {
            get { return (RepositorySection)this.Sections["repositorySection"]; }
        }

        [ConfigurationProperty("userSection")]
        public UserSection Users
        {
            get { return (UserSection)this.Sections["userSection"]; }
        }
    }
}
