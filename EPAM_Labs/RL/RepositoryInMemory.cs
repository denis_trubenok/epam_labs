﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AddressBook;
using System.Windows.Forms;
using System.Configuration;

namespace RL
{
    public class RepositoryInMemory:IRepository<AddressBook.AddressBook>
    {
        private List<AddressBook.AddressBook> _addressBookRecords;
        private List<Users> _usersRecords=new List<Users>();

        /// <summary>
        /// Property for access to _addressBookRecords
        /// </summary>
        public List<AddressBook.AddressBook> AddressBookRecords
        {
            get { return _addressBookRecords; }
            set { _addressBookRecords = value; }
        }

        /// <summary>
        /// Property for access to _usersRecords
        /// </summary>
        public List<Users> UsersRecords
        {
            get { return _usersRecords; }
            set { _usersRecords = value; }
        }

        /// <summary>
        /// Constructor of class RepositoryInMemory 
        /// </summary>
        public RepositoryInMemory()
        {
            Storage addresses = new Storage();
            _addressBookRecords = addresses.AddressBookRecords;
            UserSection usersConfiguration = (UserSection)ConfigurationManager.GetSection("userSection");
            foreach (UserElement user in usersConfiguration.Users)
            {
                _usersRecords.Add(new Users(Int32.Parse(user.IdUser), user.Login, user.Password, Int32.Parse(user.Role)));
            }
        }

        /// <summary>
        /// Method for autentification of user
        /// </summary>
        /// <param name="login">Login</param>
        /// <param name="password">Password</param>
        /// <returns></returns>
        public bool Authentication(string login, string password)
        {
            bool result = false;
            try
            {
                if (login.CompareTo("") != 0)
                {
                    var getPassword = (UsersRecords).Where(x => x.Login.CompareTo(login) == 0).Select(x => x.Password);
                    int count = 0;
                    foreach (string passw in getPassword)
                    {
                        if (password.CompareTo(passw) == 0)
                            result = true;
                        else
                            throw new Exception("Incorrect password.");
                        count++;
                    }
                    if (count == 0)
                        throw new Exception("Input your login correctly.");
                }
                else
                    throw new Exception("Enter login and password, please.");
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            return result;
        }

        /// <summary>
        /// Method for get user role by username
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        public int GetUserRole(string login)
        {
            int idRole=-1;
            var getRole = _usersRecords.Where(x => x.Login == login).Select(x=>x.IdRole);
            foreach (int role in getRole)
                idRole = role;
            return idRole;
        }

        /// <summary>
        /// Method for create new record
        /// </summary>
        /// <param name="entity">New record</param>
        public void Create(AddressBook.AddressBook entity)
        {
            _addressBookRecords.Add(entity);
        }

        /// <summary>
        /// Method for update record
        /// </summary>
        /// <param name="idRecord"></param>
        /// <param name="entity"></param>
        public void Update(int idRecord, AddressBook.AddressBook entity)
        {
            _addressBookRecords[idRecord].FirstName = entity.FirstName;
            _addressBookRecords[idRecord].SecondName = entity.SecondName;
            _addressBookRecords[idRecord].LastChangeDate = entity.LastChangeDate;
            _addressBookRecords[idRecord].Notes = entity.Notes;
            _addressBookRecords[idRecord].PhoneNumber = entity.PhoneNumber;
        }

        /// <summary>
        /// Method for delete record
        /// </summary>
        /// <param name="entity"></param>
        public void Delete(AddressBook.AddressBook entity)
        {
            if(entity!=null)
                _addressBookRecords.Remove(entity);
        }

        /// <summary>
        /// Method for find all records
        /// </summary>
        /// <returns></returns>
        public List<AddressBook.AddressBook> FindAll()
        {
            return _addressBookRecords.Select(x => x).ToList<AddressBook.AddressBook>();
        }

        /// <summary>
        /// Method find for parameter id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public List<AddressBook.AddressBook> FindForId(Guid id)
        {
            return _addressBookRecords.Where(x => x.IdRecord == id).Select(x => x).ToList<AddressBook.AddressBook>();
        }

        /// <summary>
        /// Method find for phone number records
        /// </summary>
        /// <param name="phoneNumber"></param>
        /// <returns></returns>
        public List<AddressBook.AddressBook> FindForPhoneNumber(string phoneNumber)
        {
            return _addressBookRecords.Where(x => x.PhoneNumber[0].Number.CompareTo(phoneNumber) == 0).Select(x => x).ToList<AddressBook.AddressBook>();
        }

        /// <summary>
        /// Method find for first name records
        /// </summary>
        /// <param name="firstName"></param>
        /// <returns></returns>
        public List<AddressBook.AddressBook> FindForFirstName(string firstName)
        {
            return _addressBookRecords.Where(x => x.FirstName.CompareTo(firstName) == 0).Select(x => x).ToList<AddressBook.AddressBook>();
        }

        /// <summary>
        /// Method find for second name records
        /// </summary>
        /// <param name="secondName"></param>
        /// <returns></returns>
        public List<AddressBook.AddressBook> FindForSecondName(string secondName)
        {
            return _addressBookRecords.Where(x => x.SecondName.CompareTo(secondName) == 0).Select(x => x).ToList<AddressBook.AddressBook>();
        }

        /// <summary>
        /// Method for save changes
        /// </summary>
        /// <param name="entity"></param>
        public void Save(AddressBook.AddressBook entity)
        {

        }
    }
}
