﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AddressBook;
using System.Windows.Forms;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Schema;
using System.IO;
using System.Text.RegularExpressions;
using System.Configuration;

namespace RL
{
    public class XMLRepository:IRepository<AddressBook.AddressBook>
    {
        const string XPATH1 = "/AddressBook";
        const string XPATH2 = "/AddressBook/Record";
        private List<AddressBook.AddressBook> _addressBookRecords;
        private List<Users> _usersRecords=new List<Users>();
        XMLStorage addresses;
        string _xmlFile;
        /// <summary>
        /// Constructor of class XMLRepository
        /// </summary>
        /// <param name="xmlFile">path to xml file</param>
        /// <param name="typeVerification">verification with xsd or dtd</param>
        public XMLRepository(string xmlFile, string typeVerification)
        {
            try
            {
                addresses = new XMLStorage(xmlFile, typeVerification);
                _addressBookRecords = addresses.AddressBookRecords;
                _xmlFile = xmlFile;
                XmlDocument document = new XmlDocument();
                document.Load(xmlFile);
                Document = document;

                UserSection usersConfiguration = (UserSection)ConfigurationManager.GetSection("userSection");
                foreach (UserElement user in usersConfiguration.Users)
                {
                    _usersRecords.Add(new Users(Int32.Parse(user.IdUser), user.Login, user.Password, Int32.Parse(user.Role)));
                }
            }
            catch (Exception) { }
        }

        public XmlDocument Document
        {
            get;
            private set;
        }

        /// <summary>
        /// Property for access to _addressBookRecords
        /// </summary>
        public List<AddressBook.AddressBook> AddressBookRecords
        {
            get { return _addressBookRecords; }
            set { _addressBookRecords = value; }
        }

        /// <summary>
        /// Property for access to _usersRecords
        /// </summary>
        public List<Users> UsersRecords
        {
            get { return _usersRecords; }
            set { _usersRecords = value; }
        }

        /// <summary>
        /// Method for autentification of user
        /// </summary>
        /// <param name="login">Login</param>
        /// <param name="password">Password</param>
        /// <returns></returns>
        public bool Authentication(string login, string password)
        {
            bool result = false;
            try
            {
                if (login.CompareTo("") != 0)
                {
                    var getPassword = (UsersRecords).Where(x => x.Login.CompareTo(login) == 0).Select(x => x.Password);
                    int count = 0;
                    foreach (string passw in getPassword)
                    {
                        if (password.CompareTo(passw) == 0)
                            result = true;
                        else
                            throw new Exception("Incorrect password.");
                        count++;
                    }
                    if (count == 0)
                        throw new Exception("Input your login correctly.");
                }
                else
                    throw new Exception("Enter login and password, please.");
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            return result;
        }

        /// <summary>
        /// Method for get user role by username
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        public int GetUserRole(string login)
        {
            int idRole = -1;
            var getRole = _usersRecords.Where(x => x.Login == login).Select(x => x.IdRole);
            foreach (int role in getRole)
                idRole = role;
            return idRole;
        }

        /// <summary>
        /// Method for create new record
        /// </summary>
        /// <param name="entity">New record</param>
        public void Create(AddressBook.AddressBook entity)
        {
            _addressBookRecords.Add(entity);
        }

        /// <summary>
        /// Method for update record
        /// </summary>
        /// <param name="idRecord"></param>
        /// <param name="entity"></param>
        public void Update(int idRecord, AddressBook.AddressBook entity)
        {
            _addressBookRecords[idRecord].FirstName = entity.FirstName;
            _addressBookRecords[idRecord].SecondName = entity.SecondName;
            _addressBookRecords[idRecord].LastChangeDate = entity.LastChangeDate;
            _addressBookRecords[idRecord].Notes = entity.Notes;
            _addressBookRecords[idRecord].PhoneNumber = entity.PhoneNumber;
        }

        /// <summary>
        /// Method for delete record
        /// </summary>
        /// <param name="entity"></param>
        public void Delete(AddressBook.AddressBook entity)
        {
            if (entity != null)
                _addressBookRecords.Remove(entity);
        }

        /// <summary>
        /// Method for find all records
        /// </summary>
        /// <returns></returns>
        public List<AddressBook.AddressBook> FindAll()
        {
            return _addressBookRecords;
        }

        /// <summary>
        /// Method find for parameter id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public List<AddressBook.AddressBook> FindForId(Guid id)
        {
            return _addressBookRecords.Where(x => x.IdRecord == id).Select(x => x).ToList<AddressBook.AddressBook>();
        }

        /// <summary>
        /// Method find for phone number records
        /// </summary>
        /// <param name="phoneNumber"></param>
        /// <returns></returns>
        public List<AddressBook.AddressBook> FindForPhoneNumber(string phoneNumber)
        {
            return _addressBookRecords.Where(x => x.PhoneNumber[0].Number.CompareTo(phoneNumber) == 0).Select(x => x).ToList<AddressBook.AddressBook>();
        }

        /// <summary>
        /// Method find for first name records
        /// </summary>
        /// <param name="firstName"></param>
        /// <returns></returns>
        public List<AddressBook.AddressBook> FindForFirstName(string firstName)
        {
            return _addressBookRecords.Where(x => x.FirstName.CompareTo(firstName) == 0).Select(x => x).ToList<AddressBook.AddressBook>();
        }

        /// <summary>
        /// Method find for second name records
        /// </summary>
        /// <param name="secondName"></param>
        /// <returns></returns>
        public List<AddressBook.AddressBook> FindForSecondName(string secondName)
        {
            return _addressBookRecords.Where(x => x.SecondName.CompareTo(secondName) == 0).Select(x => x).ToList<AddressBook.AddressBook>();
        }

        /// <summary>
        /// Method for save changes
        /// </summary>
        /// <param name="entity"></param>
        public void Save(AddressBook.AddressBook entity)
        {
            try
            {
                XmlNodeList nodes = Document.SelectNodes(XPATH2);
                if (AddressBookRecords.Count == nodes.Count)
                    UpdateInXML(entity, _xmlFile, Document);
                if (AddressBookRecords.Count < nodes.Count)
                    DeleteFromXML(entity, _xmlFile, Document);
                if (AddressBookRecords.Count > nodes.Count)
                    AddToXML(entity, _xmlFile, Document);
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void AddToXML(AddressBook.AddressBook entity, string xmlFile, XmlDocument document)
        {
            try
            {
                XmlNode node = document.SelectSingleNode(XPATH1);

                XmlNode record = CreateRecord(document), idRecord = CreateIdRecord(document), firstName = CreateFirstName(document, entity.FirstName);
                XmlNode secondName = CreateSecondName(document, entity.SecondName), phoneNumber;
                XmlNode note, lastChangeDate = CreateLastChangeDate(document, entity.LastChangeDate);
                record.AppendChild(idRecord);
                record.AppendChild(firstName);
                record.AppendChild(secondName);
                for (int i = 0; i < entity.PhoneNumber.Count; i++)
                {
                    phoneNumber = CreatePhoneNumber(document, entity.PhoneNumber[i].Type, entity.PhoneNumber[i].Number);
                    record.AppendChild(phoneNumber);
                }
                for (int i = 0; i < entity.Notes.Count; i++)
                {
                    note = CreateNote(document, entity.Notes[i].Tags, entity.Notes[i].String);
                    record.AppendChild(note);
                }
                record.AppendChild(lastChangeDate);
                node.AppendChild(record);
                document.AppendChild(node);
                document.Save(xmlFile);
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void UpdateInXML(AddressBook.AddressBook entity, string xmlFile, XmlDocument document)
        {
            try
            {
                XmlNodeList nodes = document.SelectNodes(XPATH2);
                XmlNode node = document.SelectSingleNode(XPATH1), old = document.SelectSingleNode(XPATH1);
                XmlNode record = CreateRecord(document), idRecord = CreateIdRecord(document), firstName = CreateFirstName(document, entity.FirstName);
                XmlNode secondName = CreateSecondName(document, entity.SecondName), phoneNumber;
                XmlNode note, lastChangeDate = CreateLastChangeDate(document, DateTime.UtcNow);
                idRecord.InnerText = Guid.NewGuid().ToString();
                idRecord.Attributes[0].Value = idRecord.InnerText;
                record.AppendChild(idRecord);
                record.AppendChild(firstName);
                record.AppendChild(secondName);
                for (int i = 0; i < entity.PhoneNumber.Count; i++)
                {
                    phoneNumber = CreatePhoneNumber(document, entity.PhoneNumber[i].Type, entity.PhoneNumber[i].Number);
                    record.AppendChild(phoneNumber);
                }
                for (int i = 0; i < entity.Notes.Count; i++)
                {
                    note = CreateNote(document, entity.Notes[i].Tags, entity.Notes[i].String);
                    record.AppendChild(note);
                }
                record.AppendChild(lastChangeDate);
                foreach (XmlElement elem in node.ChildNodes)
                {
                    if (elem.FirstChild.Attributes[0].Value.Equals(record.FirstChild.Attributes[0].Value))
                        node.ReplaceChild(record, elem);
                }
                document.ReplaceChild(node, old);
                document.Save(xmlFile);
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void DeleteFromXML(AddressBook.AddressBook entity, string xmlFile, XmlDocument document)
        {
            try
            {
                XmlNode node = document.SelectSingleNode(XPATH1);
                XmlNodeList nodes = document.SelectNodes(XPATH2);
                XmlNode record = CreateRecord(document), idRecord = CreateIdRecord(document), firstName = CreateFirstName(document, entity.FirstName);
                XmlNode secondName = CreateSecondName(document, entity.SecondName), phoneNumber;
                idRecord.InnerText = entity.IdRecord.ToString();
                int ind = 0;
                for (int i = 0; i < nodes.Count; i++)
                {
                    if (nodes[i].FirstChild.InnerText == entity.IdRecord.ToString())
                        ind = i;
                }
                XmlNode note, lastChangeDate = CreateLastChangeDate(document,
                    Convert.ToDateTime(nodes[ind].ChildNodes[nodes[ind].ChildNodes.Count - 1].Attributes[0].Value));
                idRecord.Attributes[0].Value = (entity.IdRecord).ToString();
                record.AppendChild(idRecord);
                record.AppendChild(firstName);
                record.AppendChild(secondName);
                for (int i = 0; i < entity.PhoneNumber.Count; i++)
                {
                    phoneNumber = CreatePhoneNumber(document, entity.PhoneNumber[i].Type, entity.PhoneNumber[i].Number);
                    record.AppendChild(phoneNumber);
                }
                for (int i = 0; i < entity.Notes.Count; i++)
                {
                    note = CreateNote(document, entity.Notes[i].Tags, entity.Notes[i].String);
                    record.AppendChild(note);
                }
                record.AppendChild(lastChangeDate);
                foreach (XmlElement nod in node.ChildNodes)
                {
                    if (nod.InnerText.Equals(record.InnerText))
                        node.RemoveChild(nod);
                }
                document.Save(xmlFile);
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private XmlNode CreateRecord(XmlDocument document)
        {
            XmlNode record = document.CreateElement("Record");
            return record;
        }

        private XmlNode CreateIdRecord(XmlDocument document)
        {
            XmlNodeList nodes = document.SelectNodes(XPATH2);
            XmlNode idRecord = document.CreateElement("IdRecord");
            idRecord.InnerText = Guid.NewGuid().ToString();
            XmlAttribute attrId = document.CreateAttribute("Id");
            attrId.Value = idRecord.InnerText;
            idRecord.Attributes.Append(attrId);
            return idRecord;
        }

        private XmlNode CreateFirstName(XmlDocument document,string name)
        {
            XmlNode firstName = document.CreateElement("FirstName");
            XmlAttribute attrName = document.CreateAttribute("Name");
            attrName.Value = name;
            firstName.Attributes.Append(attrName);
            firstName.InnerText = name;
            return firstName;
        }

        private XmlNode CreateSecondName(XmlDocument document,string name)
        {
            XmlNode secondName = document.CreateElement("SecondName");
            XmlAttribute attrName = document.CreateAttribute("Name");
            attrName.Value = name;
            secondName.Attributes.Append(attrName);
            secondName.InnerText = name;
            return secondName;
        }

        private XmlNode CreatePhoneNumber(XmlDocument document,string type,string number)
        {
            XmlNode phoneNumber = document.CreateElement("PhoneNumber");
            XmlAttribute attrNumber = document.CreateAttribute("Number");
            XmlAttribute attrType = document.CreateAttribute("Type");
            attrNumber.Value = number;
            attrType.Value = type;
            phoneNumber.Attributes.Append(attrType);
            phoneNumber.Attributes.Append(attrNumber);
            phoneNumber.InnerText = number;
            return phoneNumber;
        }

        private XmlNode CreateNote(XmlDocument document,List<Tag> tag,string value)
        {
            XmlNode note = document.CreateElement("Note");
            XmlNode _string = document.CreateElement("String");
            _string.InnerText = value;
            note.AppendChild(_string);
            XmlNode[] tags = new XmlNode[tag.Count];
            for (int i = 0; i < tags.Length; i++)
            {
                tags[i] = document.CreateElement("Tag");
                tags[i].InnerText = tag[i].Name;
                note.AppendChild(tags[i]);
            }
            return note;
        }

        private XmlNode CreateLastChangeDate(XmlDocument document,DateTime date)
        {
            XmlNode lastChangeDate = document.CreateElement("LastChangeDate");
            XmlAttribute attrDate = document.CreateAttribute("Date");
            attrDate.Value = date.ToString();
            lastChangeDate.Attributes.Append(attrDate);
            lastChangeDate.InnerText = date.ToString();
            return lastChangeDate;
        }
    }
}
