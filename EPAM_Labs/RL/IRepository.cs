﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RL
{
    /// <summary>
    /// Interface repository
    /// </summary>
    /// <typeparam name="TEntity">Class</typeparam>
    public interface IRepository<TEntity>
    {
        /// <summary>
        /// Method for create record
        /// </summary>
        /// <param name="entity">New record</param>
        void Create(TEntity entity);

        /// <summary>
        /// Method for update record
        /// </summary>
        /// <param name="entity">Changing record</param>
        /// <param name="newItem">New record</param>
        void Update(int idRecord, TEntity item);

        /// <summary>
        /// Method for delete record
        /// </summary>
        /// <param name="entity">Record</param>
        void Delete(TEntity entity);

        /// <summary>
        /// Method find all records
        /// </summary>
        /// <returns></returns>
        List<TEntity> FindAll();

        /// <summary>
        /// Method find for parameter id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        List<TEntity> FindForId(Guid id);

        /// <summary>
        /// Method for save changes
        /// </summary>
        /// <returns></returns>
        void Save(TEntity entity);
    }
}
