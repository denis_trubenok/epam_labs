﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AddressBook;

namespace RL
{
    /// <summary>
    /// Storage of data address book
    /// </summary>
    public class Storage
    {
        /// <summary>
        /// List of records Address book
        /// </summary>
        private List<AddressBook.AddressBook> addressBookRecords = new List<AddressBook.AddressBook>() {new AddressBook.AddressBook(Guid.NewGuid(),"Ivan","Petrov",new List<PhoneNumber>(){new PhoneNumber("work","+(37529)864-43-46")},new List<Note>(){new Note(new List<Tag>(){new Tag("first note")},"any note")},DateTime.UtcNow),
            new AddressBook.AddressBook(Guid.NewGuid(),"Admin","Adminov",new List<PhoneNumber>(),new List<Note>(){new Note(new List<Tag>(){new Tag("first note")},"any note")},DateTime.UtcNow),
            new AddressBook.AddressBook(Guid.NewGuid(),"Alex","Yvarov",new List<PhoneNumber>(){new PhoneNumber("mobile","+(37529)864-43-46")},new List<Note>(){new Note(new List<Tag>(){new Tag("first note")},"any note")},DateTime.UtcNow),
            new AddressBook.AddressBook(Guid.NewGuid(),"Sergey","Nikolaev",new List<PhoneNumber>(){new PhoneNumber("mobile","+(37525)464-43-46")},new List<Note>(){new Note(new List<Tag>(){new Tag("first note")},"any note")},DateTime.UtcNow),
            new AddressBook.AddressBook(Guid.NewGuid(),"Mihail","Sidorov",new List<PhoneNumber>(){new PhoneNumber("work","+(37544)964-43-46")},new List<Note>(){new Note(new List<Tag>(){new Tag("first note")},"any note")},DateTime.UtcNow),
            new AddressBook.AddressBook(Guid.NewGuid(),"Vladimir","Il'in",new List<PhoneNumber>(){new PhoneNumber("work","+(37533)164-43-46")},new List<Note>(){new Note(new List<Tag>(){new Tag("first note")},"any note")},DateTime.UtcNow),
            new AddressBook.AddressBook(Guid.NewGuid(),"Oleg","Yrets",new List<PhoneNumber>(){new PhoneNumber("mobile","+(37529)964-43-46")},new List<Note>(){new Note(new List<Tag>(){new Tag("first note")},"any note")},DateTime.UtcNow)};

        /// <summary>
        /// Property for access to addressBookRecords
        /// </summary>
        public List<AddressBook.AddressBook> AddressBookRecords
        {
            get { return addressBookRecords; }
            set { addressBookRecords = value; }
        }
    }
}
