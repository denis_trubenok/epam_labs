﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Schema;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using AddressBook;

namespace RL
{
    public class XMLStorage
    {
        List<AddressBook.AddressBook> _addressBookRecords;
        public XMLStorage(string xmlFile, string typeVerification)
        {
            string path0 = System.IO.Path.GetFullPath("AddressBook.dtd");
            Regex r = new Regex("task1");
            string[] ph = r.Split(path0);
            XmlDocument document = new XmlDocument();
            document.Load(xmlFile);
            switch (typeVerification)
            {
                case "dtd": {
                    if (DtdVerification(xmlFile, ph[0],document))
                        _addressBookRecords = ReadXmlFile(xmlFile,document);
                }break;
                case "xsd": {
                    if (XsdVerification(xmlFile, ph[0],document))
                        _addressBookRecords = ReadXmlFile(xmlFile,document);
                }break;
                default: { } break;
            }
        }

        /// <summary>
        /// Property for access to addressBookRecords
        /// </summary>
        public List<AddressBook.AddressBook> AddressBookRecords
        {
            get { return _addressBookRecords; }
            set { _addressBookRecords = value; }
        }

        /// <summary>
        /// Method for verification with xsd
        /// </summary>
        /// <param name="xmlFile">full path to xml file</param>
        /// <param name="path"> directory where xml file</param>
        /// <returns></returns>
        private bool XsdVerification(string xmlFile,string path, XmlDocument doc)
        {
            bool result = false;
            try
            {
                XmlElement book = (XmlElement)doc.DocumentElement.FirstChild;
                XmlNodeReader nodeReader = new XmlNodeReader(doc);
                XmlReaderSettings settings = new XmlReaderSettings();
                settings.ValidationType = ValidationType.Schema;
                settings.Schemas.Add("http://www.w3.org/2001/XMLSchema", path + "RL/AddressBook.xsd");
                settings.ValidationEventHandler += new ValidationEventHandler(ValidationCallBack);
                XmlReader reader = XmlReader.Create(nodeReader, settings);
                while (reader.Read());
                result = true;
            }
            catch (Exception) { result=false; }
            return result;
        }

        private bool DtdVerification(string xmlFile, string path, XmlDocument doc)
        {
            bool result = false;
            try
            {
                XmlElement book = (XmlElement)doc.DocumentElement.FirstChild;
                XmlNodeReader nodeReader = new XmlNodeReader(doc);
                XmlReaderSettings settings = new XmlReaderSettings();
                settings.ValidationType = ValidationType.DTD;
                settings.DtdProcessing = DtdProcessing.Parse;
                settings.Schemas.Add(null, path + "RL/AddressBook.dtd");
                settings.ValidationEventHandler += new ValidationEventHandler(ValidationCallBack);
                XmlReader reader = XmlReader.Create(nodeReader, settings);
                while (reader.Read());
                result = true;
            }
            catch (Exception) { result = false; }
            return result;
        }

        private List<AddressBook.AddressBook> ReadXmlFile(string xmlFile, XmlDocument document)
        {
            List<AddressBook.AddressBook> records = new List<AddressBook.AddressBook>();
            try
            {
                XPathNavigator navigator = document.CreateNavigator();
                navigator.MoveToFirstChild();
                navigator.MoveToNext();
                navigator.MoveToFirstChild();
                Tuple<string, XPathNavigator> attributesIdRecord, attributesFirstName, attributesSecondName;
                Tuple<List<PhoneNumber>, XPathNavigator> attributesPhoneNumber;
                Tuple<List<Note>, XPathNavigator> attributesNote;
                Tuple<DateTime, XPathNavigator> attributesLastChangeDate;
                do
                {
                    navigator.MoveToFirstChild();//<IdRecord>
                    attributesIdRecord = GetIdRecordAttribute(navigator);

                    attributesFirstName = GetFirstNameAttribute(attributesIdRecord.Item2);

                    attributesSecondName = GetSecondNameAttribute(attributesFirstName.Item2);

                    attributesPhoneNumber = GetPhoneNumberAttribute(attributesSecondName.Item2);

                    attributesNote = GetNoteAttribute(attributesPhoneNumber.Item2);

                    attributesLastChangeDate = GetLastChangeDateAttribute(attributesNote.Item2);

                    navigator = attributesLastChangeDate.Item2;
                    navigator.MoveToParent();
                    records.Add(new AddressBook.AddressBook(Guid.Parse(attributesIdRecord.Item1), attributesFirstName.Item1,
                        attributesSecondName.Item1, attributesPhoneNumber.Item1, attributesNote.Item1, attributesLastChangeDate.Item1));
                }
                while (navigator.MoveToNext());
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            return records;
        }

        private Tuple<string,XPathNavigator> GetIdRecordAttribute(XPathNavigator navigator)
        {
            string id = "";
            navigator.MoveToFirstAttribute();// attribute Id
            id = navigator.Value;
            navigator.MoveToParent();
            navigator.MoveToNext();
            return new Tuple<string, XPathNavigator>(id, navigator);
        }

        private Tuple<string, XPathNavigator> GetFirstNameAttribute(XPathNavigator navigator)
        {
            string name = "";
            navigator.MoveToFirstAttribute();//attribute Name of <FirstName>
            name = navigator.Value;
            navigator.MoveToParent();
            navigator.MoveToNext();
            return new Tuple<string, XPathNavigator>(name, navigator);
        }

        private Tuple<string, XPathNavigator> GetSecondNameAttribute(XPathNavigator navigator)
        {
            string name = "";
            navigator.MoveToFirstAttribute();//attribute Name of <SecondName>
            name = navigator.Value;
            navigator.MoveToParent();
            navigator.MoveToNext();
            return new Tuple<string, XPathNavigator>(name, navigator);
        }

        private Tuple<List<PhoneNumber>,XPathNavigator> GetPhoneNumberAttribute(XPathNavigator navigator)
        {
            string type = "", number = "";
            List<AddressBook.PhoneNumber> phones = new List<PhoneNumber>();
            while (navigator.Name.CompareTo("PhoneNumber") == 0)
            {
                navigator.MoveToFirstAttribute();//attribute Type
                type = navigator.Value;
                navigator.MoveToNextAttribute();//attribute Number
                number = navigator.Value;
                navigator.MoveToParent();
                navigator.MoveToNext();
                phones.Add(new PhoneNumber(type,number));
            }
            return new Tuple<List<PhoneNumber>,XPathNavigator>(phones,navigator);
        }

        private Tuple<List<Note>, XPathNavigator> GetNoteAttribute(XPathNavigator navigator)
        {
            List<Tag> tags = new List<Tag>();
            string _string = "";
            List<Note> notes = new List<Note>();
            while (navigator.Name.CompareTo("Note") == 0)
            {
                navigator.MoveToFirstChild();
                do
                {
                    if (navigator.Name.CompareTo("String") == 0)
                    {
                        _string = navigator.InnerXml;
                    }
                    else
                        tags.Add(new Tag(navigator.InnerXml));
                } while (navigator.MoveToNext());
                notes.Add(new Note(tags,_string));
                navigator.MoveToParent();
                navigator.MoveToNext();
            }
            return new Tuple<List<Note>, XPathNavigator>(notes, navigator);
        }

        private Tuple<DateTime, XPathNavigator> GetLastChangeDateAttribute(XPathNavigator navigator)
        {
            DateTime date;
            navigator.MoveToFirstAttribute();//attribute Date of <LastChangeDate>
            date = Convert.ToDateTime(navigator.Value);
            navigator.MoveToParent();
            return new Tuple<DateTime, XPathNavigator>(date, navigator);
        }

        private static void ValidationCallBack(object sender, ValidationEventArgs e)
        {
            throw new Exception(e.Message);
        }
    }
}
