﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace RL
{
    public class RepositorySection:ConfigurationSection
    {
        /// <summary>
        /// Collection of Users
        /// </summary>
        [ConfigurationProperty("repositories")]
        public RepositoriesCollection Repositories
        {
            get { return (RepositoriesCollection)this["repositories"]; }
        }
    }
}
