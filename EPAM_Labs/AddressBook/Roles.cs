﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AddressBook
{
    /// <summary>
    /// Class for storage records of user roles
    /// </summary>
    public class Roles
    {
        private int idRole;
        private string nameOfRole;

        /// <summary>
        /// Property for access to idRole
        /// </summary>
        public int IdRole
        {
            get { return idRole; }
            set{idRole=value;}
        }

        /// <summary>
        /// Property for access to nameOfRole
        /// </summary>
        public string NameOfRole
        {
            get { return nameOfRole; }
            set { nameOfRole = value; }
        }
    }
}
