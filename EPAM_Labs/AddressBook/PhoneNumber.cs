﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AddressBook
{
    public class PhoneNumber
    {
        string _type, _number;

        public PhoneNumber(string type,string number)
        {
            _type = type; _number = number;
        }

        public PhoneNumber()
        {        }

        public string Type
        {
            get { return _type; }
            set { _type = value; }
        }

        public string Number
        {
            get { return _number; }
            set { _number = value; }
        }
    }
}
