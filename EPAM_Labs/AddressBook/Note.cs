﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AddressBook
{
    public class Note
    {
        public Note(List<Tag> tag, string str)
        {
            Tags = tag; String = str;
        }

        public Note()
        {        }

        public List<Tag> Tags
        {  get;set;    }

        public string String
        {  get; set;    }
    }
}
