﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AddressBook
{
    /// <summary>
    /// Class for storage records of addresses
    /// </summary>
    public class AddressBook
    {
        private Guid idRecord;
        string firstName, secondName;
        List<PhoneNumber> phoneNumber;
        List<Note> notes;
        DateTime lastChangeDate;

        /// <summary>
        /// Constructor of Class AddressBook
        /// </summary>
        /// <param name="IdRecord"></param>
        /// <param name="FirstName"></param>
        /// <param name="SecondName"></param>
        /// <param name="PhoneNumber"></param>
        /// <param name="Notes"></param>
        /// <param name="LastChangeDate"></param>
        public AddressBook(Guid IdRecord,string FirstName,string SecondName,List<PhoneNumber> PhoneNumber,List<Note> Notes,DateTime LastChangeDate)
        {
            idRecord = IdRecord; firstName = FirstName; secondName = SecondName; phoneNumber = PhoneNumber;
            notes = Notes; lastChangeDate = LastChangeDate;
        }

        /// <summary>
        /// Property for access to idRecord
        /// </summary>
        public Guid IdRecord
        {
            get { return idRecord; }
            set {idRecord=value; }
        }

        /// <summary>
        /// Property for access to firstName
        /// </summary>
        public string FirstName
        {
            get { return firstName; }
            set { firstName = value; }
        }

        /// <summary>
        /// Property for access to secondName
        /// </summary>
        public string SecondName
        {
            get { return secondName; }
            set { secondName = value; }
        }

        /// <summary>
        /// Property for access to notes
        /// </summary>
        public List<Note> Notes
        {
            get { return notes; }
            set { notes = value; }
        }

        /// <summary>
        /// Property for access to phoneNumber
        /// </summary>
        public List<PhoneNumber> PhoneNumber
        {
            get { return phoneNumber; }
            set { phoneNumber = value; }
        }

        /// <summary>
        /// Property for access to lastChangeTime
        /// </summary>
        public DateTime LastChangeDate
        {
            get { return lastChangeDate; }
            set { lastChangeDate = value; }
        }
    }
}
