﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AddressBook
{
    /// <summary>
    /// Class for storage records of user
    /// </summary>
    public class Users
    {
        string login, password;
        int idUser,idRole;
        /// <summary>
        /// Constructor of class Users
        /// </summary>
        /// <param name="IdUser"></param>
        /// <param name="Login"></param>
        /// <param name="Password"></param>
        /// <param name="IdRole"></param>
        public Users(int IdUser,string Login, string Password,int IdRole)
        {
            idUser=IdUser; login = Login; idRole = IdRole; password = Password;
        }

        /// <summary>
        /// Property for access to login 
        /// </summary>
        public string Login
        {
            get { return login;}
            set { login = value; }
        }

        /// <summary>
        /// Property for access to password
        /// </summary>
        public string Password
        {
            get { return password; }
            set { password = value; }
        }

        /// <summary>
        /// Property for access to idRole
        /// </summary>
        public int IdRole
        {
            get { return idRole; }
            set { idRole = value; }
        }

        /// <summary>
        /// Property for access to idUser
        /// </summary>
        public int IdUser
        {
            get { return idUser; }
            set { idUser = value; }
        }
    }
}
