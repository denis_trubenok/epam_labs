﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AddressBook
{
    public class Tag
    {
        public Tag(string name)
        {
            Name = name;
        }

        public Tag()
        { }

        public string Name
        {  get; set; }
    }
}
