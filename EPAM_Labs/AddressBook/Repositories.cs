﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AddressBook
{
    public class Repositories
    {
        public Repositories(string name, string location,string type)
        {
            Name = name; Location = location; Type = type;
        }

        public string Name
        {
            get;
            private set;
        }

        public string Location
        {
            get;
            private set;
        }

        public string Type
        {
            get;
            private set;
        }
    }
}
