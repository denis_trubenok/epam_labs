﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Threading;
using System.Globalization;
using System.Configuration;
using AddressBook;
using RL;

namespace task1
{
    /// <summary>
    /// Логика взаимодействия для AuthorizationWindow.xaml
    /// </summary>
    public partial class AuthorizationWindow : Window
    {
        public AuthorizationWindow()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");
            //Thread.CurrentThread.CurrentUICulture = new CultureInfo("ru-RU");
            //Thread.CurrentThread.CurrentUICulture = new CultureInfo("fr-FR");
            InitializeComponent();
        }

        /// <summary>
        /// Method which autentificate and autorizate user on button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LoginButton_Click(object sender, RoutedEventArgs e)
        {
            RepositoryInMemory memory = new RepositoryInMemory();
            if (memory.Authentication(LoginTextBox.Text, PasswordBox.Password))
            {
                AddressBookWindow wind = new AddressBookWindow();
                wind.Owner = this;
                wind.ShowDialog();
                Close();
            }
        }

        /// <summary>
        /// Method which change localize on change select
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LocaleComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (LocaleComboBox.SelectedIndex)
            {
                case 0:
                    {
                        Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                        Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");
                    } break;
                case 1:
                    {
                        Thread.CurrentThread.CurrentCulture = new CultureInfo("fr-FR");
                        Thread.CurrentThread.CurrentUICulture = new CultureInfo("fr-FR");
                    } break;
                case 2:
                    {
                        Thread.CurrentThread.CurrentCulture = new CultureInfo("ru-RU");
                        Thread.CurrentThread.CurrentUICulture = new CultureInfo("ru-RU");
                    } break;
                default: MessageBox.Show("ChooseLocation"); break;
            }
        }
    }
}
