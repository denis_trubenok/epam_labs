﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Threading;
using System.Globalization;
using System.Windows.Markup;
using System.Text.RegularExpressions;
using System.IO;
using AddressBook;
using RL;
using System.Configuration;

namespace task1
{
    /// <summary>
    /// Логика взаимодействия для AddressBookWindow.xaml
    /// </summary>
    public partial class AddressBookWindow : Window
    {
        public AddressBookWindow()
        {
            FrameworkElement.LanguageProperty.OverrideMetadata(
                typeof(FrameworkElement),
                new FrameworkPropertyMetadata(
                    XmlLanguage.GetLanguage(CultureInfo.CurrentCulture.IetfLanguageTag)));
            InitializeComponent();
        }

        public int buttonIsChecked = -1,//this variable to display the button is pressed to add or change
            idSelectedRecord = -1,//id record which selected for update
            typeSeclectedRepository=0;//if 0 - repository in memory, if 1 xml repository
        AuthorizationWindow window;
        public RepositoryInMemory memory = new RepositoryInMemory();
        public XMLRepository xmlRepository;
        public List<AddressBook.AddressBook> addressBookRecords;
        public AddressBook.AddressBook aTemp;

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            window = (AuthorizationWindow)this.Owner;
            DataGrid.IsReadOnly = true;
            if (memory.GetUserRole(window.LoginTextBox.Text) == 2)//if user is administrator
                VisibilityControls();
            Refresh();//method for refrash table
        }

        /// <summary>
        /// Method for set controls visible
        /// </summary>
        private void VisibilityControls()
        {
            UpdateButton.Visibility = Visibility.Visible;
            DeleteButton.Visibility = Visibility.Visible;
            AdditionButton.Visibility = Visibility.Visible;
        }

        /// <summary>
        /// Method for refresh table DataGrid
        /// </summary>
        private void Refresh()
        {
            if(typeSeclectedRepository==0)
                addressBookRecords = memory.FindAll();
            else
                addressBookRecords = xmlRepository.FindAll();
            foreach (AddressBook.AddressBook address in addressBookRecords)
            {
                address.LastChangeDate = address.LastChangeDate.ToLocalTime();//change data to local
            }
            DataGrid.DataContext = addressBookRecords;
        }

        /// <summary>
        /// Method on change selected row
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((AddressBook.AddressBook)DataGrid.SelectedItem != null)
            {
                aTemp = (AddressBook.AddressBook)DataGrid.SelectedItem; idSelectedRecord = DataGrid.SelectedIndex;
                ViewRecordButton.Visibility = Visibility.Visible;
            }
            else
                ViewRecordButton.Visibility = Visibility.Hidden;
        }

        /// <summary>
        /// Method which add new record on button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AdditionButton_Click(object sender, RoutedEventArgs e)
        {
            buttonIsChecked = 1;
            EditWindow wind = new EditWindow();
            wind.Owner = this;
            wind.ShowDialog();
            MessageLabel.Content = "";
            Refresh();
        }

        /// <summary>
        /// Method which delete record on button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            if (aTemp != null)
            {
                MessageLabel.Content = "";
                if (typeSeclectedRepository == 0)
                    memory.Delete(aTemp);
                else
                {
                    xmlRepository.Delete(aTemp);
                    xmlRepository.Save(aTemp);
                }
                Refresh(); aTemp = null;
            }
            else
                MessageLabel.Content = "Choose any record in table for delete";
        }

        /// <summary>
        /// Method which find record for parameter on button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FindButton_Click(object sender, RoutedEventArgs e)
        {
            if (typeSeclectedRepository == 0)
                FindInMemoryRepository();
            else
                FindInXMLRepository();
        }

        /// <summary>
        /// Metho for update record
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            if (aTemp != null)
            {
                MessageLabel.Content = "";
                buttonIsChecked = 3;
                EditWindow wind = new EditWindow();
                wind.Owner = this;
                wind.ShowDialog();
                Refresh(); aTemp = null;
            }
            else
                MessageLabel.Content = "Choose any record in table for update";
        }

        private void LoadRepositoryButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                List<Repositories> _listOfReps = new List<Repositories>();
                RepositorySection repositories = (RepositorySection)ConfigurationManager.GetSection("repositorySection");
                foreach (RepositoryElement repository in repositories.Repositories)
                {
                    _listOfReps.Add(new Repositories(repository.Name,repository.Location,repository.Type));
                }
                switch (ChooseRepositoryComboBox.SelectedIndex)
                {
                    case 0:
                        {
                            typeSeclectedRepository = 0;
                        } break;
                    case 1:
                        {
                            typeSeclectedRepository = 1;
                            xmlRepository = new XMLRepository(_listOfReps[0].Location, "xsd");
                        } break;
                    case 2:
                        {
                            typeSeclectedRepository = 1;
                            xmlRepository = new XMLRepository(_listOfReps[1].Location, "xsd");
                        } break;
                }
                MessageLabel.Content = "";
                Refresh();
            }
            catch (Exception)
            {
                MessageLabel.Content = "This repository is not valid or not exist. Because now is load repository in memory.";
                typeSeclectedRepository = 0;
                Refresh();
            }
        }

        private void FindInMemoryRepository()
        {
            List<AddressBook.AddressBook> records = new List<AddressBook.AddressBook>();
            switch (FindComboBox.SelectedIndex)
            {
                case 0://if parameter first name
                    {
                        if (FindTextbox.Text.CompareTo("") != 0)
                            records = memory.FindForFirstName(FindTextbox.Text);
                    } break;
                case 1://if parameter second name
                    {
                        if (FindTextbox.Text.CompareTo("") != 0)
                            records = memory.FindForSecondName(FindTextbox.Text);
                    } break;
                case 2://if parameter phone number
                    {
                        if (FindTextbox.Text.CompareTo("") != 0)
                            records = memory.FindForPhoneNumber(FindTextbox.Text);
                    } break;
                default:
                    {

                    } break;
            }
            if (FindTextbox.Text.CompareTo("") == 0)
            {
                records = addressBookRecords;
            }
            DataGrid.DataContext = records;
        }

        private void FindInXMLRepository()
        {
            List<AddressBook.AddressBook> records = new List<AddressBook.AddressBook>();
            switch (FindComboBox.SelectedIndex)
            {
                case 0://if parameter first name
                    {
                        if (FindTextbox.Text.CompareTo("") != 0)
                            records = xmlRepository.FindForFirstName(FindTextbox.Text);
                    } break;
                case 1://if parameter second name
                    {
                        if (FindTextbox.Text.CompareTo("") != 0)
                            records = xmlRepository.FindForSecondName(FindTextbox.Text);
                    } break;
                case 2://if parameter phone number
                    {
                        if (FindTextbox.Text.CompareTo("") != 0)
                            records = xmlRepository.FindForPhoneNumber(FindTextbox.Text);
                    } break;
                default:
                    {

                    } break;
            }
            if (FindTextbox.Text.CompareTo("") == 0)
            {
                records = addressBookRecords;
            }
            DataGrid.DataContext = records;
        }

        private void ViewRecordButton_Click(object sender, RoutedEventArgs e)
        {
            if (aTemp != null)
            {
                MessageLabel.Content = "";
                buttonIsChecked = 2;
                EditWindow wind = new EditWindow();
                wind.Owner = this;
                wind.ShowDialog();
                Refresh(); aTemp = null;
            }
            else
                MessageLabel.Content = "Choose any record in table for show it";
        }
    }

}
