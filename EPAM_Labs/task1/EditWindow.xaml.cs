﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Text.RegularExpressions;
using AddressBook;
using RL;

namespace task1
{
    /// <summary>
    /// Логика взаимодействия для EditWindow.xaml
    /// </summary>
    public partial class EditWindow : Window
    {
        public EditWindow()
        {
            InitializeComponent();
        }

        AddressBookWindow window;

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            window = (AddressBookWindow)this.Owner;
            switch(window.buttonIsChecked)
            {
                case 1:
                    {
                        VisibilityControlsIsAdding();
                    }break;
                case 2:
                    {
                        VisibilityControlsIsShowRecord();
                        InsertVariablesForShowRecord();
                    }break;
                case 3:
                    {
                        VisibilityControlsIsUpdate();
                        InsertVariablesForUpdate();
                    }break;
                default:{}break;
            }
        }

        /// <summary>
        /// Method to insert variables for update record
        /// </summary>
        private void InsertVariablesForUpdate()
        {
            if (window.aTemp != null)
            {
                FirstNameTextBox.Text = window.aTemp.FirstName;
                SecondNameTextBox.Text = window.aTemp.SecondName;
                dataGridPhoneNumbers.DataContext = window.aTemp.PhoneNumber;
                dataGridNotes.IsReadOnly = false;
                dataGridPhoneNumbers.IsReadOnly = false;
                dataGridNotes.DataContext = window.aTemp.Notes;
                dataGridNotes.AutoGenerateColumns = false;
                dataGridPhoneNumbers.AutoGenerateColumns = false;
            }
        }

        private void InsertVariablesForShowRecord()
        {
            if (window.aTemp != null)
            {
                ContentFirstNameLabel.Content = window.aTemp.FirstName;
                ContentSecondNameLabel.Content = window.aTemp.SecondName;
                dataGridPhoneNumbers.DataContext = window.aTemp.PhoneNumber;
                dataGridNotes.DataContext = window.aTemp.Notes;
            }
        }

        /// <summary>
        /// Method for create record
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AdditionButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (CorrectlyData(FirstNameTextBox.Text, SecondNameTextBox.Text, PhoneNumberTextBox.Text, NoteTextBox.Text))
                {
                    AddressBook.AddressBook record = new AddressBook.AddressBook(Guid.NewGuid(), FirstNameTextBox.Text, SecondNameTextBox.Text, new List<AddressBook.PhoneNumber>() { new AddressBook.PhoneNumber("work", PhoneNumberTextBox.Text) },
                           new List<AddressBook.Note>() { new AddressBook.Note(new List<Tag>() { new Tag("first note") }, NoteTextBox.Text) }, DateTime.UtcNow);
                    if (window.typeSeclectedRepository == 0)
                        window.memory.Create(record);
                    else
                    {
                        window.xmlRepository.Create(record);
                        window.xmlRepository.Save(record);
                    }
                    Close();
                }
                else
                    MessageLabel.Content = "Not correctly filled out the field: " + TextMessage();
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
                
        }

        /// <summary>
        /// Method for update record
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (CorrectlyData(FirstNameTextBox.Text, SecondNameTextBox.Text, window.aTemp.PhoneNumber[0].Number, window.aTemp.Notes[0].String))
                {
                    if (window.typeSeclectedRepository == 0)
                        window.memory.Update(window.idSelectedRecord,
                        new AddressBook.AddressBook(window.aTemp.IdRecord, FirstNameTextBox.Text, SecondNameTextBox.Text, window.aTemp.PhoneNumber, window.aTemp.Notes, DateTime.UtcNow));
                    else
                    {
                        window.xmlRepository.Update(window.idSelectedRecord,
                            new AddressBook.AddressBook(window.aTemp.IdRecord, FirstNameTextBox.Text, SecondNameTextBox.Text, window.aTemp.PhoneNumber, window.aTemp.Notes, DateTime.UtcNow));
                        window.xmlRepository.Save(new AddressBook.AddressBook(window.aTemp.IdRecord, FirstNameTextBox.Text, SecondNameTextBox.Text, window.aTemp.PhoneNumber, window.aTemp.Notes, DateTime.UtcNow));
                    }
                    Close();
                }
                else
                    MessageLabel.Content = "Not correctly filled out the fields: " + TextMessage();
            }
            catch {}
        }

        /// <summary>
        /// Method for verification input data 
        /// </summary>
        /// <returns></returns>
        private bool CorrectlyData(string firstName, string secondName, string phoneNumber, string notes)
        {
            Regex[] regular = new Regex[] { new Regex(@"\S\S\d{1,5}\S\d{3}-\d{2}-\d{2}"), new Regex(@"\d?\S\d{3,6}\S\d{1,3}-\d{2}-\d{2}") };
            bool result = false;
            if (firstName.CompareTo("") != 0 && secondName.CompareTo("") != 0)
                if (regular[0].IsMatch(phoneNumber) == true || regular[1].IsMatch(phoneNumber) == true)
                    result = true;
            return result;
        }

        /// <summary>
        /// Method for allert message 
        /// </summary>
        /// <returns></returns>
        private string TextMessage()
        {
            string result = "";
            Regex[] regular = new Regex[] { new Regex(@"\S?\S\d{1,6}\S\d{1,3}-\d{2}-\d{2}"), new Regex(@"\d?\S\d{3,6}\S\d{1,3}-\d{2}-\d{2}") };
            if (FirstNameTextBox.Text.CompareTo("") == 0)
                result += "FirstName";
            if (SecondNameTextBox.Text.CompareTo("") == 0)
                result += " SecondName";
            if (regular[0].IsMatch(PhoneNumberTextBox.Text) == false)
                result += " PhoneNumber";
            return result;
        }

        private void DeletePhonenumberButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if ((AddressBook.PhoneNumber)dataGridPhoneNumbers.SelectedItem != null)
                {
                    window.aTemp.PhoneNumber.Remove((AddressBook.PhoneNumber)dataGridPhoneNumbers.SelectedItem);
                    Refresh();
                }
            }
            catch { }
        }

        private void Refresh()
        {
            if (window.aTemp.PhoneNumber.Count==0)
                dataGridPhoneNumbers.DataContext = new List<PhoneNumber>() {new PhoneNumber("home","")};
            if (window.aTemp.Notes.Count == 0)
                dataGridNotes.DataContext = new List<AddressBook.Note>() { new Note(new List<Tag>(){new Tag("")}, "") };
            dataGridPhoneNumbers.DataContext=window.aTemp.PhoneNumber;
            dataGridNotes.DataContext = window.aTemp.Notes;
        }

        private void DeleteNoteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if ((AddressBook.Note)dataGridNotes.SelectedItem != null)
                {
                    window.aTemp.Notes.Remove((AddressBook.Note)dataGridNotes.SelectedItem);
                    Refresh();
                }
            }
            catch { }
        }

        private void dataGridPhoneNumbers_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
        }

        private void dataGridNotes_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void VisibilityControlsIsAdding()
        {
            AdditionButton.Visibility = Visibility.Visible;
            dataGridNotes.Visibility = Visibility.Hidden;
            dataGridPhoneNumbers.Visibility = Visibility.Hidden;
        }

        private void VisibilityControlsIsUpdate()
        {
            UpdateButton.Visibility = Visibility.Visible;
            dataGridNotes.Visibility = Visibility.Visible;
            dataGridPhoneNumbers.Visibility = Visibility.Visible;
            PhoneNumberLabel.Visibility = Visibility.Hidden;
            PhoneNumberTextBox.Visibility = Visibility.Hidden;
            NoteLabel.Visibility = Visibility.Hidden;
            NoteTextBox.Visibility = Visibility.Hidden;
        }

        private void VisibilityControlsIsShowRecord()
        {
            dataGridNotes.Visibility = Visibility.Visible;
            dataGridPhoneNumbers.Visibility = Visibility.Visible;
            FirstNameTextBox.Visibility = Visibility.Hidden;
            SecondNameTextBox.Visibility = Visibility.Hidden;
            PhoneNumberLabel.Visibility = Visibility.Hidden;
            PhoneNumberTextBox.Visibility = Visibility.Hidden;
            NoteLabel.Visibility = Visibility.Hidden;
            NoteTextBox.Visibility = Visibility.Hidden;
            ColumnDeleteNote.Visibility = Visibility.Hidden;
            ColumnDeletePhone.Visibility = Visibility.Hidden;
            dataGridNotes.AutoGenerateColumns = false;
            dataGridPhoneNumbers.AutoGenerateColumns = false;
            dataGridNotes.IsReadOnly=true;
            dataGridPhoneNumbers.IsReadOnly = true;
        }
    }
}
